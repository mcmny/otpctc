class IonFinder(object):
    def __init__(self, input_file_path='', output_file_path='outfile.txt', interesting_events_file = ''):
        super(IonFinder, self).__init__()
        self.interesting_events_file = interesting_events_file
        self.input_file_path = input_file_path
        self.output_file_path = output_file_path
        self.found_ions = []
        self.interesting_events = []

    def open_list(self):
        with open(self.interesting_events_file) as event_file:
            for line in event_file:
                event_name = line.split()[0].split('/')[-1]
                self.interesting_events.append(event_name)
                print event_name

    def do_things(self):
        with open(self.input_file_path) as infile:
            for line in infile:
                if line.split()[0].split('/')[-1] in self.interesting_events:
                    self.found_ions.append(line)

    def save(self):
        with open(self.output_file_path, 'a') as outfile:
            for ion in self.found_ions:
                outfile.write(ion)

    def find_ions(self):
        self.open_list()
        print(self.interesting_events)
        self.do_things()
        self.save()





