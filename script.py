from finders import IonFinder

ionfinder = IonFinder(
    input_file_path='./test_files/input.txt',
    output_file_path='./out2.txt',
    interesting_events_file='./test_files/interesting.txt'
)
ionfinder.find_ions()